package lv.jansevskis.martins.otraispraktiskaisdarbs;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    /**
     * Open dailog with checkboxes
     * @param view
     */
    public void openDialog(View view) {
        Toast.makeText(this, "Open dialog", Toast.LENGTH_LONG).show();
        SelectMultiple dialogDemo = new SelectMultiple();
        dialogDemo.show(getSupportFragmentManager(), "example");
    }

    /**
     * Opens second activity
     * @param view
     */
    public void goToActivitySecond(View view) {
        Intent intent = new Intent(MainActivity.this, Main2Activity.class);
        startActivity(intent);
    }
}
