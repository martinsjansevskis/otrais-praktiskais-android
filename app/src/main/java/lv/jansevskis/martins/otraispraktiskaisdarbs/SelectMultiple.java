package lv.jansevskis.martins.otraispraktiskaisdarbs;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.widget.Toast;

import java.util.ArrayList;


public class SelectMultiple extends DialogFragment {


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final String[] members = getResources().getStringArray(R.array.groupMembers);

        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setTitle(R.string.dialogTitle);
        builder.setMultiChoiceItems(R.array.groupMembers, null, new DialogInterface.OnMultiChoiceClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                if (isChecked) {
                    Toast.makeText(getActivity(), members[which] + " izvēlēts", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(getActivity(), members[which] + " NEizvēlēts", Toast.LENGTH_LONG).show();
                }
            }
        })
            .setPositiveButton(R.string.okButton, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    Toast.makeText(getActivity(), "Labi poga", Toast.LENGTH_LONG).show();
                }
            })
            .setNegativeButton(R.string.cancelButton, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    Toast.makeText(getActivity(), "Atcelt poga", Toast.LENGTH_LONG).show();
                }
        });

        // Create the AlertDialog object and return it
        return builder.create();
    }
}
